<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"
    isELIgnored="false"
    %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Product Details</title>
</head>
<body>
<br/>
<center>
<h3>WELCOME TO PRODUCT DATA PAGE</h3>
</center>
<center>${message}</center>
<br/>
<a href="index.html" style="margin-left: 850px;">Back</a>
<br/><br/>
<center>
<table border="1">
	<tr>
		<th>ID</th>
		<th>CODE</th>
		<th>COST</th>
		<th>DISCOUNT</th>
		<th>GST</th>
		<th colspan="2">OPERATION</th>
	</tr>
	<c:forEach items="${list}" var="ob">
		<tr>
			<td>${ob.prodId}</td>
			<td>${ob.prodCode}</td>
			<td>${ob.prodCost}</td>
			<td>${ob.prodDiscount}</td>
			<td>${ob.prodGst}</td>
			<td><a href="delete?pid=${ob.prodId}">DELETE</a></td>
			<td><a href="edit?pid=${ob.prodId}">EDIT</a></td>
		</tr>
	</c:forEach>
</table>
</center>

</body>
</html>


