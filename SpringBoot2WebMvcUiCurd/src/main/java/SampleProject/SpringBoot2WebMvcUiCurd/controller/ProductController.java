package SampleProject.SpringBoot2WebMvcUiCurd.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import SampleProject.SpringBoot2WebMvcUiCurd.model.Product;
import SampleProject.SpringBoot2WebMvcUiCurd.service.IProductService;

@Controller
public class ProductController {
	@Autowired
	private IProductService service;

	//1. Display Register page
	@RequestMapping("/register")
	public String showPage() {
		return "ProductRegister";
	}

	//2. Read Form data as ModelAttribute and save
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public String saveData(
			@ModelAttribute Product product,
			Model model
			) 
	{
		//save data in DB
		Integer id=service.saveProduct(product);
		//create message
		String msg="Product ID '"+id+"' saved";
		//send to UI
		model.addAttribute("message", msg);
		//Goto UI Page back
		return "ProductRegister";
	}

	//3. Fetch all records from DB and display at UI
	@RequestMapping("/all")
	public String fetchAll(Model model) {
		//read data from DB
		List<Product> list= service.getAllProducts();
		//send data to UI
		model.addAttribute("list", list);
		return "ProductData";
	}

	//4. Delete row by Id   .../delete?pid=10
	@RequestMapping("/delete")
	public String deleteOne(
			@RequestParam("pid")Integer id,
			Model model
			) 
	{
		service.deleteProduct(id);
		model.addAttribute("message", "Product ID '"+id+"' Deleted");
		//send remaining data
		//read data from DB
		List<Product> list= service.getAllProducts();
		//send data to UI
		model.addAttribute("list", list);
		return "ProductData";
	}

	//5. show edit page with data
	@RequestMapping("/edit")
	public String showEdit(
			@RequestParam("pid")Integer id,
			Model model
			)
	{
		Product p=service.getOneProduct(id);
		//send data to UI and fill in HTML FORM
		model.addAttribute("ob", p);
		return "ProductEdit";
	}

	//6. Update Product
	@RequestMapping(value = "/update",method = RequestMethod.POST)
	public String updateOne(
			@ModelAttribute Product product,
			Model model
			) 
	{
		service.updateProduct(product);
		//send data to UI
		model.addAttribute("message", "Product ID  '"+product.getProdId()+"' Updated");
		//send othre data
		List<Product> list=service.getAllProducts();
		model.addAttribute("list", list);
		return "ProductData";
	}
	
	
	
}