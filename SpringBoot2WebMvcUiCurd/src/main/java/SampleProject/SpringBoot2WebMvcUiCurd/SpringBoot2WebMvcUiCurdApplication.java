package SampleProject.SpringBoot2WebMvcUiCurd;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBoot2WebMvcUiCurdApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBoot2WebMvcUiCurdApplication.class, args);
	}

}
