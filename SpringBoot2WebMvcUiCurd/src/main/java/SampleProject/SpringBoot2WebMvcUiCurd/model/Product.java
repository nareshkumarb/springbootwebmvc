package SampleProject.SpringBoot2WebMvcUiCurd.model;

import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.Data;

@Data
@Entity
public class Product {
	@Id
	private Integer prodId;
	private String prodCode;
	private Double prodCost;
	private Double prodDiscount;
	private Double prodGst;
	
	public Integer getProdId() {
		return prodId;
	}
	public void setProdId(Integer prodId) {
		this.prodId = prodId;
	}
	public String getProdCode() {
		return prodCode;
	}
	public void setProdCode(String prodCode) {
		this.prodCode = prodCode;
	}
	public Double getProdCost() {
		return prodCost;
	}
	public void setProdCost(Double prodCost) {
		this.prodCost = prodCost;
	}
	public Double getProdDiscount() {
		return prodDiscount;
	}
	public void setProdDiscount(Double prodDiscount) {
		this.prodDiscount = prodDiscount;
	}
	public Double getProdGst() {
		return prodGst;
	}
	public void setProdGst(Double prodGst) {
		this.prodGst = prodGst;
	}
	
	
}


