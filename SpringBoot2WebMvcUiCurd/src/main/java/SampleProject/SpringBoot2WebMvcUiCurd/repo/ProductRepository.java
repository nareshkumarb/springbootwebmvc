package SampleProject.SpringBoot2WebMvcUiCurd.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import SampleProject.SpringBoot2WebMvcUiCurd.model.Product;

public interface ProductRepository 
	extends JpaRepository<Product, Integer> 
{

}
